import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
class App {
  constructor() {
    //1. Вызывает логотип загрузки 
    //2. Отображает героев
    this.startApp();
  }
  // Селекторы для отображения 
  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      // Отображение логотипа на время загрузки героев
      App.loadingElement.style.visibility = 'visible';
      // получить всех героев массивом обьекто (id, image) через API 
      const fighters = await fighterService.getFighters();
      // 
      const fightersElement = createFighters(fighters);
      // Отображаем героев в блоке html
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      // Убираем логотип загрузки
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
