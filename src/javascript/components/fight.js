import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  let firstPlayerComboCooldown = Date.now() - 10000;
  let secondPlayerComboCooldown = Date.now() - 10000;

  let firstFighterHealthLeft = firstFighter.health;
  let secondFighterHealthLeft = secondFighter.health;

  const fightersBar = document.querySelectorAll('.arena___health-bar');
  const showDamage = document.querySelector('.arena___damageBlock');

  const [
    firstFighterBar,
    secondFighterBar
  ] = fightersBar;

  const {
    PlayerOneAttack,
    PlayerOneBlock,
    PlayerTwoAttack,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination
  } = controls;

  const [q, w, e] = PlayerOneCriticalHitCombination,
    [u, i, o] = PlayerTwoCriticalHitCombination;

  return new Promise((resolve) => {
    const pressed = new Set();
    document.addEventListener('keydown', (event) => {
      let combo;
      pressed.add(event.code);
      switch (true) {
        case pressed.has(q) && pressed.has(w) && pressed.has(e):
          combo = criticalDamage(firstFighter, pressed, firstPlayerComboCooldown);
          if (combo) {
            firstPlayerComboCooldown = combo.cooldown;
            secondFighterHealthLeft -= combo.critical;
            fighterHealthInPercents(secondFighterHealthLeft, secondFighter, secondFighterBar)
            secondFighterHealthLeft <= 0 && resolve(firstFighter);
          }
          break;
        case pressed.has(u) && pressed.has(i) && pressed.has(o):
          combo = criticalDamage(secondFighter, pressed, secondPlayerComboCooldown);
          if (combo) {
            secondPlayerComboCooldown = combo.cooldown;
            firstFighterHealthLeft -= combo.critical;
            fighterHealthInPercents(firstFighterHealthLeft, firstFighter, firstFighterBar)
            firstFighterHealthLeft <= 0 && resolve(secondFighter);
          }
          break;
        case pressed.has(PlayerOneBlock):
        case pressed.has(PlayerTwoBlock):
          document.addEventListener('keyup', (event) => {
            event.code === PlayerOneBlock && pressed.delete(PlayerOneBlock);
            event.code === PlayerTwoBlock && pressed.delete(PlayerTwoBlock);
          })
          break;
        case pressed.has(PlayerOneAttack):
          secondFighterHealthLeft = onKeydown(firstFighter, secondFighter, secondFighterHealthLeft, showDamage, 'arena___first-fighter-hit');
          fighterHealthInPercents(secondFighterHealthLeft, secondFighter, secondFighterBar);
          secondFighterHealthLeft <= 0 && resolve(firstFighter);
          pressed.delete(PlayerOneAttack);
          break;
        case pressed.has(PlayerTwoAttack) && !pressed.has(PlayerTwoBlock):
          firstFighterHealthLeft = onKeydown(secondFighter, firstFighter, firstFighterHealthLeft, showDamage, 'arena___second-fighter-hit');
          fighterHealthInPercents(firstFighterHealthLeft, firstFighter, firstFighterBar);
          firstFighterHealthLeft <= 0 && resolve(secondFighter);
          pressed.delete(PlayerTwoAttack);
          break;
        default:
          break;
      }
    })
  });
}

function criticalDamage(fighter, set, cooldown) {
  const currentTime = Date.now();
  if (currentTime - cooldown < 10000) {
    set.clear();
    return false;
  }
  const critical = fighter.attack * 2;
  cooldown = currentTime;
  set.clear();

  return {
    critical,
    cooldown
  };
}

function fighterHealthInPercents(target, fighter, healthBar) {
  const barWidthInPercents = (target * 100) / fighter.health;
  healthBar.style.width = `${barWidthInPercents}%`;
}

function onKeydown(atacker, defender, fighterHealthLeft, showDamage, style) {
  const damage = getDamage(atacker, defender);
  showDamage.className = `arena___damageBlock ${style}`;
  if (!damage) {
    showDamage.innerHTML = 'Blocked';
    return fighterHealthLeft;
  } else {
    showDamage.innerHTML = damage.toFixed(3);
    fighterHealthLeft -= damage

    return fighterHealthLeft > 0 ? fighterHealthLeft : 0;
  }
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}
