import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right arena___right-fighter' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const details = createElement({
    tagName: 'ul',
    className: 'fighter-details'
  })
  if (fighter) {
    for (let el in fighter) {
      if (el !== '_id' && el !== 'source') {
        const row = createElement({
          tagName: 'li',
        })
        row.innerHTML = `${el} : ${fighter[el]}`
        details.append(row)
      } else if (el === 'source') {
        const row = createFighterImage(fighter);
        row.classList.add('short-size');
        fighterElement.append(row)
      }
    }
    fighterElement.prepend(details)
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });
  return imgElement;
}