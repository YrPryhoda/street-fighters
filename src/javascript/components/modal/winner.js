import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const winnerTitle = `${fighter.name} has won this time`;
  const winnerDescription = `The game will be reloaded after you close modal window`

  showModal({
    title: winnerTitle,
    bodyElement: winnerDescription,
    onClose: () => window.location.reload()
  });
}
